# README #

### Entrega tarea 1 ###
La mayoría de los cambios fueron hechos en el archivo thread.c, para el scheduler se modificó el método next_thread_to_run_dq, se elige el thread activo de mayor prioridad. En thread_ticks se cuentan las estadisticas y se revisa la expiración de quantums. El sistema de DQ esta implementado como colas virtuales, se guardan todos los thread ready en la cola ready_list y a la estructura se le agrego un identificador (current_queue) que indica si están en la cola Active o Expired.

###Cosas que funcionan y como probarlas###
* Cambiar la prioridad y hacer un cambio de contexto si es necesario: Se puede probar con el test priority-change

* Scheduler con expropiación: se puede probar con priority-preempt

* Double queue scheduler: un ejemplo para probarlo es usar el test priority-preempt2, que hace lo mismo que priority-preempt pero con 1000 iteraciones (es recomendable enviar el output a un archivo), lo que aqui pasa es que en algún momento (muy variable dependiendo de si se comenta o no, etc)  el thread high_priority agota sus 2 quantum y pasa a la cola expired, por lo que se le da prioridad para que termine el thread main, una vez que este termina, no quedan threads activos y se vuelve a hacer activos los threads expired (en este caso high-priority), por lo que termina después del main.

* Es importante mecionar que funciona el test priority-fifo, ya que esto es importante para el flujo de todos los programas, ya que si hay 2+ threads con la misma prioridad, los atiende FIFO pero con quantum's, por lo que funciona correctamente round-robin.

### Entrega tarea 2 ###

### Se implementaron las siguientes syscalls: ###

* Syscall_exit
* Syscall_halt
* Syscall_exec