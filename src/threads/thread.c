#include "threads/thread.h"
#include <debug.h>
#include <stddef.h>
#include <random.h>
#include <stdio.h>
#include <string.h>
#include "threads/flags.h"
#include "threads/interrupt.h"
#include "threads/intr-stubs.h"
#include "threads/palloc.h"
#include "threads/switch.h"
#include "threads/synch.h"
#include "threads/vaddr.h"
#ifdef USERPROG
#include "userprog/process.h"
#endif

#include "devices/shutdown.h"


/* Random value for struct thread's `magic' member.
   Used to detect stack overflow.  See the big comment at the top
   of thread.h for details. */
#define THREAD_MAGIC 0xcd6abf4b

//#define COMMENTS_ON

#ifdef COMMENTS_ON
static char printer[500] = "\0";

static char blocking_printer[500] = "\0";
static int should_print_blocking_printer = 0;

#endif


static int times_unblocked = 0;


static int executed_threads = 0;
static int total_waiting_time = 0;
static int context_changes = 0;
static char final_stat_printer[500] = "\0";
static int should_print_final_stats = 0;

static char stat_printer[500] = "\0";
static int  should_print_stats = 0;




/* List of processes in THREAD_READY state, that is, processes
   that are ready to run but not actually running. */
static struct list ready_list;

/* List of all processes.  Processes are added to this list
   when they are first scheduled and removed when they exit. */
static struct list all_list;

/* Idle thread. */
static struct thread *idle_thread;

/* Initial thread, the thread running init.c:main(). */
static struct thread *initial_thread;

/* Lock used by allocate_tid(). */
static struct lock tid_lock;

/* Stack frame for kernel_thread(). */
struct kernel_thread_frame
  {
    void *eip;                  /* Return address. */
    thread_func *function;      /* Function to call. */
    void *aux;                  /* Auxiliary data for function. */
  };

/* Statistics. */
static long long idle_ticks;    /* # of timer ticks spent idle. */
static long long kernel_ticks;  /* # of timer ticks in kernel threads. */
static long long user_ticks;    /* # of timer ticks in user programs. */

/* Scheduling. */
#define TIME_SLICE 4            /* # of timer ticks to give each thread. */
static unsigned thread_ticks;   /* # of timer ticks since last yield. */

/* This selects the used scheduling algorithm.
 * 'Hot Swapping' function is not required and also not expected.
 * Selection is controlled by the kernel command-line
 *  option "-o SCHEDULER" where SCHEDULER can be:
 *   - "FCFS" or "FIFO" (default)
 *   - "prioritized"
 *   - "sCFS"
 */
enum scheduling_algorithm selected_scheduler = SCH_FCFS;
next_thread_function next_thread_to_run_function;

static void kernel_thread (thread_func *, void *aux);

static void idle (void *aux UNUSED);
static struct thread *running_thread (void);
static struct thread *next_thread_to_run (void);
static void init_thread (struct thread *, const char *name, int priority);
static bool is_thread (struct thread *) UNUSED;
static void *alloc_frame (struct thread *, size_t size);
static void schedule (void);
void thread_schedule_tail (struct thread *prev);
static tid_t allocate_tid (void);

/* Initializes the threading system by transforming the code
   that's currently running into a thread.  This can't work in
   general and it is possible in this case only because loader.S
   was careful to put the bottom of the stack at a page boundary.

   Also initializes the run queue and the tid lock.

   After calling this function, be sure to initialize the page
   allocator before trying to create any threads with
   thread_create().

   It is not safe to call thread_current() until this function
   finishes. */
void
thread_init (void)
{
  ASSERT (intr_get_level () == INTR_OFF);

  lock_init (&tid_lock);
  list_init (&ready_list);
  list_init (&all_list);

  pick_scheduler();

  /* Set up a thread structure for the running thread. */
  initial_thread = running_thread ();

  #ifdef COMMENTS_ON
  printf("thread inicial: %s, priority: %d", initial_thread->name, initial_thread->priority);
  #endif

  init_thread (initial_thread, "main", PRI_DEFAULT);
  initial_thread->status = THREAD_RUNNING;
  initial_thread->tid = allocate_tid ();
}

/* Starts preemptive thread scheduling by enabling interrupts.
   Also creates the idle thread. */
void
thread_start (void)
{
  /* Create the idle thread. */
  struct semaphore idle_started;
  sema_init (&idle_started, 0);
  thread_create ("idle", PRI_MIN, idle, &idle_started);

  /* Start preemptive thread scheduling. */
  intr_enable ();

  /* Wait for the idle thread to initialize idle_thread. */
  sema_down (&idle_started);
}

/* Called by the timer interrupt handler at each timer tick.
   Thus, this function runs in an external interrupt context. */
void
thread_tick (void)
{
  struct thread *t = thread_current ();


  /* Update statistics. */
  if (t == idle_thread)
    idle_ticks++;
#ifdef USERPROG
  else if (t->pagedir != NULL)
    user_ticks++;
#endif
  else
    kernel_ticks++;

  t->exec_time++;
  thread_ready_foreach(add1_to_active, 0);
  thread_foreach(add1_to_blocked,0);

  /* Enforce preemption. */

  if (++thread_ticks >= TIME_SLICE){ 
    t->quantum_expired++;
    t->total_quantum_expired++;
    #ifdef COMMENTS_ON
    printf("El thread %s ha completado un quantum, le quedan %d quantums para expirar y en total lleva %d quantums completados a lo largo de su ejecucion\n",t->name, (2- t->quantum_expired), t->total_quantum_expired);
    #endif
    if(t->quantum_expired >= 2){

      #ifdef COMMENTS_ON
      printf("El thread %s ha expirado, pasará a la cola de expirados\n", t->name);
      #endif
      t->current_queue = EXPIRED;
      t->quantum_expired = 0;
    }
    
    intr_yield_on_return ();

  }

}

/* Prints thread statistics. */
void
thread_print_stats (void)
{
  printf ("Thread: %lld idle ticks, %lld kernel ticks, %lld user ticks\n",
          idle_ticks, kernel_ticks, user_ticks);
}

/* Creates a new kernel thread named NAME with the given initial
   PRIORITY, which executes FUNCTION passing AUX as the argument,
   and adds it to the ready queue.  Returns the thread identifier
   for the new thread, or TID_ERROR if creation fails.

   If thread_start() has been called, then the new thread may be
   scheduled before thread_create() returns.  It could even exit
   before thread_create() returns.  Contrariwise, the original
   thread may run for any amount of time before the new thread is
   scheduled.  Use a semaphore or some other form of
   synchronization if you need to ensure ordering.

   The code provided sets the new thread's `priority' member to
   PRIORITY, but no actual priority scheduling is implemented.
   Priority scheduling is the goal of Problem 1-3. */
tid_t
thread_create (const char *name, int priority,
               thread_func *function, void *aux)
{
  struct thread *t;
  struct kernel_thread_frame *kf;
  struct switch_entry_frame *ef;
  struct switch_threads_frame *sf;
  tid_t tid;
  enum intr_level old_level;

  ASSERT (function != NULL);

  /* Allocate thread. */
  t = palloc_get_page (PAL_ZERO);
  if (t == NULL)
    return TID_ERROR;

  /* Initialize thread. */
  init_thread (t, name, priority);
  tid = t->tid = allocate_tid ();

  /* Prepare thread for first run by initializing its stack.
     Do this atomically so intermediate values for the 'stack'
     member cannot be observed. */
  old_level = intr_disable ();

  /* Stack frame for kernel_thread(). */
  kf = alloc_frame (t, sizeof *kf);
  kf->eip = NULL;
  kf->function = function;
  kf->aux = aux;

  /* Stack frame for switch_entry(). */
  ef = alloc_frame (t, sizeof *ef);
  ef->eip = (void (*) (void)) kernel_thread;

  /* Stack frame for switch_threads(). */
  sf = alloc_frame (t, sizeof *sf);
  sf->eip = switch_entry;
  sf->ebp = 0;

  intr_set_level (old_level);

  /* Add to run queue. */
  thread_unblock (t);



  /* custom */

  #ifdef COMMENTS_ON
  printf("__________El thread %s ha entrado a pasado a la cola %d con prioridad %d, status: %d\n",
   t->name, t->priority,t->current_queue ,t->status);

  old_level = intr_disable ();
  printf("__________El estado actual de la cola READY es:\n");
  thread_ready_foreach(print_thread,0);
  printf("__________El estado actual de la cola ALL es:\n");
  thread_foreach(print_thread,0);

  intr_set_level (old_level);
  #endif
  
  if(t != idle_thread)
    thread_yield ();


  return tid;
}

/* Puts the current thread to sleep.  It will not be scheduled
   again until awoken by thread_unblock().

   This function must be called with interrupts turned off.  It
   is usually a better idea to use one of the synchronization
   primitives in synch.h. */
void
thread_block (void)
{
  ASSERT (!intr_context ());
  ASSERT (intr_get_level () == INTR_OFF);

  struct thread * t = thread_current ();
  t->status = THREAD_BLOCKED;
  t->times_blocked++;

  #ifdef COMMENTS_ON
  if(t == idle_thread)
    should_print_blocking_printer = 0;
  else{
    snprintf(blocking_printer, 400, "__________________se ha bloqueado el thread %s\n",t->name);
    should_print_blocking_printer = 1;
  }
  #endif

  schedule ();

  #ifdef COMMENTS_ON
  if (should_print_blocking_printer)
    printf("%s", blocking_printer);
  #endif

}

/* Transitions a blocked thread T to the ready-to-run state.
   This is an error if T is not blocked.  (Use thread_yield() to
   make the running thread ready.)

   This function does not preempt the running thread.  This can
   be important: if the caller had disabled interrupts itself,
   it may expect that it can atomically unblock a thread and
   update other data. */
void
thread_unblock (struct thread *t)
{
  enum intr_level old_level;

  ASSERT (is_thread (t));


  old_level = intr_disable ();
  ASSERT (t->status == THREAD_BLOCKED);
  list_push_back (&ready_list, &t->elem);
  t->status = THREAD_READY;
  t->current_queue = ACTIVE;

  intr_set_level (old_level);

  times_unblocked++;

}

/* Returns the name of the running thread. */
const char *
thread_name (void)
{
  return thread_current ()->name;
}

/* Returns the running thread.
   This is running_thread() plus a couple of sanity checks.
   See the big comment at the top of thread.h for details. */
struct thread *
thread_current (void)
{
  struct thread *t = running_thread ();

  /* Make sure T is really a thread.
     If either of these assertions fire, then your thread may
     have overflowed its stack.  Each thread has less than 4 kB
     of stack, so a few big automatic arrays or moderate
     recursion can cause stack overflow. */
  ASSERT (is_thread (t));
  ASSERT (t->status == THREAD_RUNNING);

  return t;
}

/* Returns the running thread's tid. */
tid_t
thread_tid (void)
{
  return thread_current ()->tid;
}

/* Deschedules the current thread and destroys it.  Never
   returns to the caller. */
void
thread_exit (void)
{
  ASSERT (!intr_context ());

#ifdef USERPROG
  process_exit ();
#endif

  /* Remove thread from all threads list, set our status to dying,
     and schedule another process.  That process will destroy us
     when it calls thread_schedule_tail(). */
  intr_disable ();

  struct thread * t = thread_current();
  list_remove (&t->allelem);

  t->status = THREAD_DYING;

  /*Guardamos en un string los stats y hacemos verdadera la booleana para que se imprima al terminar schedule*/
  snprintf(&stat_printer, 300 ,"\n\n______________________\nEl thread %s ha muerto:\n-Estuvo %d milisegundos ejecutando\n-Estuvo %d milisegundos esperando en la cola active\n-Estuvo %d milisegundos bloqueado\n-Fue %d veces elegido para ejecutar\n-Expiro %d veces su quantum\n-Fue bloqueado %d veces\n_________________________\n\n",
    t->name, (t->exec_time)*10, (t->active_time)*10, (t->blocked_time)*10, t->times_chosen, t->total_quantum_expired, t->times_blocked);
  should_print_stats = 1;

  if(list_size(&all_list) <= 1){
      executed_threads++;
      total_waiting_time += (t->active_time)*10;

      snprintf(&final_stat_printer, 300, "\n\n______________________\nHa terminado la ejecucion del programa: \n-En total se han ejecutado %d threads (sin contar idle), \n-El tiempo de espera promedio en la cola active fue de %d/%d\n-Se realizaron %d cambios de conexto.\n______________________\n\n",
        executed_threads, total_waiting_time,executed_threads, context_changes );
      should_print_final_stats = 1;

  }else{
      executed_threads++;
      total_waiting_time += (t->active_time)*10;
  }



  schedule ();
  NOT_REACHED ();


}

/* Yields the CPU.  The current thread is not put to sleep and
   may be scheduled again immediately at the scheduler's whim. */
void
thread_yield (void)
{
  struct thread *cur = thread_current ();
  enum intr_level old_level;

  ASSERT (!intr_context ());
  ASSERT (cur->status == THREAD_RUNNING);

  old_level = intr_disable ();

  if (cur != idle_thread)
    list_push_back (&ready_list, &cur->elem);
  cur->status = THREAD_READY;
  schedule ();
  intr_set_level (old_level);
}

/* Invoke function 'func' on all threads, passing along 'aux'.
   This function must be called with interrupts off. */
void
thread_foreach (thread_action_func *func, void *aux)
{
  struct list_elem *e;

  ASSERT (intr_get_level () == INTR_OFF);

  for (e = list_begin (&all_list); e != list_end (&all_list);
       e = list_next (e))
    {
      struct thread *t = list_entry (e, struct thread, allelem);
      func (t, aux);
    }
}

void
thread_ready_foreach(thread_action_func *func, void *aux)
{
  struct list_elem *e;

  ASSERT (intr_get_level () == INTR_OFF);

  for (e = list_begin (&ready_list); e != list_end (&ready_list);
       e = list_next (e))
    {
      struct thread *t = list_entry (e, struct thread, elem);
      func (t, aux);
    }
}

/* Sets the current thread's priority to NEW_PRIORITY. */
void
thread_set_priority (int new_priority)
{
  thread_current ()->priority = new_priority;
  thread_yield();
}

/* Returns the current thread's priority. */
int
thread_get_priority (void)
{
  return thread_current ()->priority;
}

/* Sets the current thread's nice value to NICE. */
void
thread_set_nice (int nice UNUSED)
{
  /* Not yet implemented. */
}

/* Returns the current thread's nice value. */
int
thread_get_nice (void)
{
  /* Not yet implemented. */
  return 0;
}

/* Returns 100 times the system load average. */
int
thread_get_load_avg (void)
{
  /* Not yet implemented. */
  return 0;
}

/* Returns 100 times the current thread's recent_cpu value. */
int
thread_get_recent_cpu (void)
{
  /* Not yet implemented. */
  return 0;
}

/* Idle thread.  Executes when no other thread is ready to run.

   The idle thread is initially put on the ready list by
   thread_start().  It will be scheduled once initially, at which
   point it initializes idle_thread, "up"s the semaphore passed
   to it to enable thread_start() to continue, and immediately
   blocks.  After that, the idle thread never appears in the
   ready list.  It is returned by next_thread_to_run() as a
   special case when the ready list is empty. */
static void
idle (void *idle_started_ UNUSED)
{
  struct semaphore *idle_started = idle_started_;
  idle_thread = thread_current ();
  sema_up (idle_started);

  for (;;)
    {
      /* Let someone else run. */
      intr_disable ();
      thread_block ();

      /* Re-enable interrupts and wait for the next one.

         The `sti' instruction disables interrupts until the
         completion of the next instruction, so these two
         instructions are executed atomically.  This atomicity is
         important; otherwise, an interrupt could be handled
         between re-enabling interrupts and waiting for the next
         one to occur, wasting as much as one clock tick worth of
         time.

         See [IA32-v2a] "HLT", [IA32-v2b] "STI", and [IA32-v3a]
         7.11.1 "HLT Instruction". */
      asm volatile ("sti; hlt" : : : "memory");
    }
}

/* Function used as the basis for a kernel thread. */
static void
kernel_thread (thread_func *function, void *aux)
{
  ASSERT (function != NULL);

  intr_enable ();       /* The scheduler runs with interrupts off. */
  function (aux);       /* Execute the thread function. */
  thread_exit ();       /* If function() returns, kill the thread. */
}

/* Returns the running thread. */
struct thread *
running_thread (void)
{
  uint32_t *esp;

  /* Copy the CPU's stack pointer into `esp', and then round that
     down to the start of a page.  Because `struct thread' is
     always at the beginning of a page and the stack pointer is
     somewhere in the middle, this locates the curent thread. */
  asm ("mov %%esp, %0" : "=g" (esp));
  return pg_round_down (esp);
}

/* Returns true if T appears to point to a valid thread. */
static bool
is_thread (struct thread *t)
{
  return t != NULL && t->magic == THREAD_MAGIC;
}

/* Does basic initialization of T as a blocked thread named
   NAME. */
static void
init_thread (struct thread *t, const char *name, int priority)
{
  ASSERT (t != NULL);
  ASSERT (PRI_MIN <= priority && priority <= PRI_MAX);
  ASSERT (name != NULL);

  memset (t, 0, sizeof *t);
  t->status = THREAD_BLOCKED;
  strlcpy (t->name, name, sizeof t->name);
  t->stack = (uint8_t *) t + PGSIZE;
  t->priority = priority;
  t->magic = THREAD_MAGIC;
  t->exec_time = 0;
  t->active_time = 0;
  t->blocked_time = 0;
  t->times_chosen = 0;
  t->times_blocked = 0;
  t->total_quantum_expired = 0;
  list_push_back (&all_list, &t->allelem);
}

/* Allocates a SIZE-byte frame at the top of thread T's stack and
   returns a pointer to the frame's base. */
static void *
alloc_frame (struct thread *t, size_t size)
{
  /* Stack data is always allocated in word-size units. */
  ASSERT (is_thread (t));
  ASSERT (size % sizeof (uint32_t) == 0);

  t->stack -= size;
  return t->stack;
}


/** First Come, First Serve. */
static struct thread *
next_thread_to_run_fcfs (void)
{
  return list_entry (list_pop_front (&ready_list), struct thread, elem);
}

/** Multi-level feedback queue scheduler */
static struct thread *
next_thread_to_run_mlfqs (void)
{
  //unimplemented, just for completeness
  //SOMEDAY: implement, nah :P
  return list_entry (list_pop_front (&ready_list), struct thread, elem);
}

/** Prioritized Scheduling
 *
 * Should avoid deadlocks when a priority process waits for another with
 *  lower priority.
 */
static struct thread *
next_thread_to_run_prioritized (void)
{
  //OLDFIXME: Actually it performs FCFS (FIFO).
  //OLDTODO: Implement a prioritized scheduler.
  return list_entry (list_pop_front (&ready_list), struct thread, elem);
}

/** Simplified implementation of the Completely Fair Scheduler */
static struct thread *
next_thread_to_run_sCFS (void)
{
  //OLDFIXME: Actually it performs FCFS (FIFO).
  //OLDTODO: Implement Simple Completely Fair Scheduler
  return list_entry (list_pop_front (&ready_list), struct thread, elem);
}

/** Lottery Scheduler */
static struct thread *
next_thread_to_run_lottery (void)
{
  //FIXME: Actually it performs FCFS (FIFO).
  //TODO: Implement Prioritized Lottery Scheduler
  return list_entry (list_pop_front (&ready_list), struct thread, elem);
}

/** Dynamic Lottery Scheduler */
static struct thread *
next_thread_to_run_dyn_Lottery (void)
{
  //FIXME: Actually it performs FCFS (FIFO).
  //TODO: Implement Dynamic Priority Lottery Scheduler
  return list_entry (list_pop_front (&ready_list), struct thread, elem);
}

/** Dual Queue Scheduler */
static struct thread *
next_thread_to_run_dq (void)
{
  //FIXME: Actually it performs FCFS (FIFO).
  //TODO: Implement Dual Queue Scheduler

  //esta línea devuelve el elemento activo del thread con mayor prioridad
  struct list_elem  * element = list_max (&ready_list, less_active_max, 0);

  //este es el thread activo con mayor prioridad
  struct thread * out = list_entry (element, struct thread, elem);


  //si el thread devuelto fue expired, entonces la cola active está vacía y se cambian todos a active y se vuelve a buscar
  if(out->current_queue == EXPIRED){
    struct thread * old_out = out;
    thread_foreach(from_expired_to_active,0);
    element = list_max (&ready_list, less_active_max,0);
    out = list_entry (element, struct thread, elem);
    
    #ifdef COMMENTS_ON
    snprintf(&printer, 400, "Se obtuvo el thread %s, expirado, luego se intercambian las colas y se obtuvo el thread %s con prioridad %d\n"
      , old_out->name ,out->name, out->priority);
    #endif

  }
  #ifdef COMMENTS_ON
  else{
    snprintf(&printer, 400, "Se obtuvo el thread %s, activo, con prioridad %d, status: %d\n"
    ,out->name, out->priority, out->status);
  }
  #endif
  //se saca de la lista ya que pasara a estar running pero sigue estando activo
  list_remove(element);

  out->times_chosen++;
  return out;
}

void
pick_scheduler(void)
{
  switch(selected_scheduler){
    case SCH_MLFQS:
      printf("Using MLFQS as the CPU Scheduler\n");
      next_thread_to_run_function = next_thread_to_run_mlfqs;
      break;
    case SCH_PRIORITIZED:
      printf("Using a prioritized CPU Scheduler\n");
      next_thread_to_run_function = next_thread_to_run_prioritized;
      break;
    case SCH_SIMPLE_CFS:
      printf("Using a simple implementation of the CFS as the CPU Scheduler\n");
      next_thread_to_run_function = next_thread_to_run_sCFS;
      break;
    case SCH_LOTTERY:
      printf("Using prioritized Lottery as the CPU Scheduler\n");
      next_thread_to_run_function = next_thread_to_run_lottery;
      break;
    case SCH_DYN_LOTTERY:
      printf("Using a dynamic prioritized Lottery as the CPU Scheduler\n");
      next_thread_to_run_function = next_thread_to_run_dyn_Lottery;
      break;
    case SCH_DQ:
      printf("Using a dual queue CPU Scheduler\n");
      next_thread_to_run_function = next_thread_to_run_dq;
      break;
    case SCH_FCFS:
    default:
      printf("Using FCFS as the CPU Scheduler\n");
      next_thread_to_run_function = next_thread_to_run_fcfs;
      break;
  }
}

/* Chooses and returns the next thread to be scheduled.  Should
   return a thread from the run queue, unless the run queue is
   empty.  (If the running thread can continue running, then it
   will be in the run queue.)  If the run queue is empty, return
   idle_thread. */  

static struct thread *
next_thread_to_run (void)
{
  //NOTE: This default behaviour should be moved into the next thread to run
  //        functions if not all of them need this.
  if (list_empty (&ready_list))
    return idle_thread;

  return next_thread_to_run_function();
}

/* Completes a thread switch by activating the new thread's page
   tables, and, if the previous thread is dying, destroying it.

   At this function's invocation, we just switched from thread
   PREV, the new thread is already running, and interrupts are
   still disabled.  This function is normally invoked by
   thread_schedule() as its final action before returning, but
   the first time a thread is scheduled it is called by
   switch_entry() (see switch.S).

   It's not safe to call printf() until the thread switch is
   complete.  In practice that means that printf()s should be
   added at the end of the function.

   After this function and its caller returns, the thread switch
   is complete. */
void
thread_schedule_tail (struct thread *prev)
{
  struct thread *cur = running_thread ();

  ASSERT (intr_get_level () == INTR_OFF);

  /* Mark us as running. */
  cur->status = THREAD_RUNNING;

  /* Start new time slice. */
  if(prev!=NULL || cur == idle_thread){
    thread_ticks = 0;
  }
#ifdef USERPROG
  /* Activate the new address space. */
  process_activate ();
#endif

  /* If the thread we switched from is dying, destroy its struct
     thread.  This must happen late so that thread_exit() doesn't
     pull out the rug under itself.  (We don't free
     initial_thread because its memory was not obtained via
     palloc().) */
  if (prev != NULL && prev->status == THREAD_DYING && prev != initial_thread)
    {
      ASSERT (prev != cur);
      palloc_free_page (prev);
    }
}

/* Schedules a new process.  At entry, interrupts must be off and
   the running process's state must have been changed from
   running to some other state.  This function finds another
   thread to run and switches to it.

   It's not safe to call printf() until thread_schedule_tail()
   has completed. */
static void
schedule (void)
{
  struct thread *cur = running_thread ();
  struct thread *next = next_thread_to_run ();
  struct thread *prev = NULL;

  ASSERT (intr_get_level () == INTR_OFF);
  ASSERT (cur->status != THREAD_RUNNING);
  ASSERT (is_thread (next));

  if (cur != next){
    prev = switch_threads (cur, next);
    context_changes++;
  }
  thread_schedule_tail (prev);


  if(should_print_stats){
    printf("%s\n",stat_printer);
    should_print_stats = 0;
  }

  if(should_print_final_stats){
    printf("%s\n", final_stat_printer);
    should_print_final_stats = 0;
  }

  #ifdef COMMENTS_ON
  if(printer[0] != 0){
    printf("%s\n", printer);
    printer[0] = 0;
  }
  #endif

}

/* Returns a tid to use for a new thread. */
static tid_t
allocate_tid (void)
{
  static tid_t next_tid = 1;
  tid_t tid;

  lock_acquire (&tid_lock);
  tid = next_tid++;
  lock_release (&tid_lock);

  return tid;
}

_Bool less_active_max(const struct list_elem * e1, const struct list_elem * e2, void * aux UNUSED){

    struct thread * t1 = list_entry (e1, struct thread, elem);
    struct thread * t2 = list_entry (e2, struct thread, elem);

    ASSERT(t1->status != THREAD_BLOCKED);
    ASSERT(t2->status != THREAD_BLOCKED);

    int p1 = t1->priority;
    int p2 = t2->priority;
    
    //si el maximo actual está expirado, y el thread que se está mirando esta active, se devuelve true sin importar
    //si es mayor o menor, si no es así se devuelve el mayor
    /*
    t1 active, t2 active, p1 > p2 -> FALSE
    t1 active, t2 active, p1 < p2 -> TRUE
    t1 active, t2 expired, p1 < p2 -> FALSE
    t1 active, t2 expired, p1 > p2 -> FALSE
    t1 expired, t2 active, p1 > p2 -> TRUE
    t1 expired, t2 active, p1 < p2 -> TRUE
    t1 expired, t2 expired, p1 < p2 -> FALSE
    t1 expired, t2 expired, p1 > p2 -> FALSE
    */
    if(t1->current_queue == EXPIRED && t2->current_queue == ACTIVE)
      return true;
    else 
      return (t2->current_queue == ACTIVE) && (p1 < p2);
}

void print_thread(struct thread * t, void * aux UNUSED ){
  printf("__________-nombre thread: %s, priority: %d, queue: %d, status: %d\n", t->name, t->priority, t->current_queue, t->status);
}

//método que aumenta en 1 el contador de tiempo que indica cuanta espera lleva en la cola active
void add1_to_active(struct thread * t, void * aux UNUSED){
  if(t->current_queue == ACTIVE)
    t->active_time++;
}

void add1_to_blocked(struct thread * t, void * aux UNUSED){
  if(t->status == THREAD_BLOCKED)
    t->blocked_time++;
}

void from_expired_to_active(struct thread * t, void * aux UNUSED){
  t->current_queue = ACTIVE;
}


/* Offset of `stack' member within `struct thread'.
   Used by switch.S, which can't figure it out on its own. */
uint32_t thread_stack_ofs = offsetof (struct thread, stack);
