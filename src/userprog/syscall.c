#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "devices/shutdown.h"
#include <user/syscall.h>
#include "threads/vaddr.h"
#include "userprog/process.h"


// Implementation Parameters
// =========================
static int WRITE_SIZE=128;



inline
int
syscall_write(struct intr_frame *f)
{
  int *p = f->esp;  // Stack pointer (x86 registers http://www.scs.stanford.edu/05au-cs240c/lab/i386/s02_03.htm)

  //FIXME: This is (probably) displaced because of the -12 on process.c:442 (setup_stack)
  //int file_descriptor =         p[1];
  //char        *buffer = (char*) p[2];
  //int            size = (int)   p[3];  // huge size may cause overflow

  int file_descriptor =         p[5];
  char        *buffer = (char*) p[6];
  int            size = (int)   p[7];  // may overflow signed int!!

  switch(file_descriptor)
  {
    case STDIN_FILENO:
      // Inform that no data was written
      f->eax=0;
      // REVIEW: Should that process be terminated?
      return 2;

    case STDOUT_FILENO:
    {
      // Write in chunks of WRITE_SIZE
      //   putbuf (src/lib/kernel/console.c) locks the console,
      //   we should avoid locking it too often or for too long
      int remaining = size;
      while(remaining > WRITE_SIZE)
      {
        // Write a chunk
        putbuf(buffer, WRITE_SIZE);
        // Advance buffer pointer
        buffer    += WRITE_SIZE;
        remaining -= WRITE_SIZE;
      }
      // Write all the remaining data
      putbuf(buffer, remaining);

      // Inform the amount of data written
      f->eax=(int)size;
      return 0;
    }

    default:
      printf("syscall: write call not implemented for files\n");
      return 1;
  }

  return 1;  // Unreachable, but compiler complains
}

inline
void
syscall_halt (void)
{
  shutdown_power_off();
}

inline
void
syscall_exit (int status)
{
  printf("%s: exit(%d)\n", thread_current()->name, status);
  thread_exit();
}

static int
get_user (const uint8_t *uaddr)
{
  int result;
  asm ("movl $1f, %0; movzbl %1, %0; 1:"
       : "=&a" (result) : "m" (*uaddr));
  return result;
}

static int get_four_bytes_user(const void * add) {
  if(add >= PHYS_BASE) 
    { 
      syscall_exit(-1); 
    }

  uint8_t *uaddr = (uint8_t *) add;

  int temp;
  temp = get_user(uaddr);

  int resultado = 0;

  if(temp == -1)
    syscall_exit(-1); 
  resultado += (temp << 0);
  temp = get_user(uaddr + 1);

  if(temp == -1) 
    syscall_exit(-1);
  resultado += (temp << 8);
  temp = get_user(uaddr + 2);

  if(temp == -1) 
    syscall_exit(-1);
  resultado += (temp << 16);
  temp = get_user(uaddr + 3);

  if(temp == -1) 
    syscall_exit(-1);
  resultado += (temp << 24);

  return resultado;
}

inline
pid_t syscall_exec(const char *cmd_line) 
{
  tid_t child_tid;
  
  if ((void *)cmd_line >= PHYS_BASE || get_user((void *)cmd_line) == -1) {
      syscall_exit(-1);
      return -1;
  }
  
  child_tid = process_execute(cmd_line);

  return child_tid;
}

static void
syscall_handler (struct intr_frame *f)
{
  // intr_frame holds CPU register data
  //   Intel 80386 Reference Programmer's Manual (TL;DR)
  //     http://www.scs.stanford.edu/05au-cs240c/lab/i386/toc.htm

  int *p = f->esp;  // Stack pointer (x86 registers http://www.scs.stanford.edu/05au-cs240c/lab/i386/s02_03.htm)
  int syscall_number = (*p);

  switch(syscall_number)
  {
    case SYS_HALT:
      syscall_halt();
      return;

    case SYS_EXIT:
      syscall_exit(get_four_bytes_user(p + 4));
      NOT_REACHED();

    case SYS_EXEC:
      f->eax = (uint32_t) syscall_exec((const char *) get_four_bytes_user(p + 4));
      return;

    case SYS_WAIT:
      printf("system call: wait\n");
      break;

    case SYS_WRITE:
      syscall_write(f);
      return;

    default:
      printf("system call: unhandled syscall. Terminating process[%d]\n",
             thread_current()->tid);
      break;
  }

  // Syscall handling failed, terminate the process
  thread_exit ();
}



void
syscall_init (void)
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

